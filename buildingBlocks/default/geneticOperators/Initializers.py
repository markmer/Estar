"""Инициализация популяции"""
# from buildingBlocks.baseline.GeneticOperators import GeneticOperatorIndivid, GeneticOperatorPopulation
from buildingBlocks.baseline.BasicEvolutionaryEntities import GeneticOperatorIndivid, GeneticOperatorPopulation
from buildingBlocks.default.geneticOperators.supplementary.Other import check_operators_from_kwargs, apply_decorator


class InitIndivid(GeneticOperatorIndivid):
    def __init__(self, params=None):
        super().__init__(params=params)

    @apply_decorator
    def apply(self, individ, *args, **kwargs) -> None:
        individ.apply_operator('MutationIndivid')


class InitPopulation(GeneticOperatorPopulation):
    def __init__(self, params):
        super().__init__(params=params)
        self._check_params('population_size', 'individ')

    def apply(self, population, *args, **kwargs):
        population.structure = []
        for _ in range(self.params['population_size']):
            new_individ = self.params['individ'].copy()
            new_individ.apply_operator('InitIndivid')
            population.structure.append(new_individ)
        return population
